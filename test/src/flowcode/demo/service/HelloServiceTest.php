<?php

namespace flowcode\demo\service;

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2013-07-05 at 18:06:43.
 */
class HelloServiceTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var HelloService
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        $this->object = new HelloService;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }

    /**
     * @covers flowcode\demo\service\HelloService::getHelloMessage
     * @todo   Implement testGetHelloMessage().
     */
    public function testGetHelloMessage() {
        $message = $this->object->getHelloMessage();
        $this->assertEquals("setup message: hi", $message);
    }

}

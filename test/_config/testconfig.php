<?php

use flowcode\wing\mvc\Config;

/* data base */
Config::set("database", array(
    "driver" => "mysql",
    "mapping" => "orm-mapping.xml",
    "dbserver" => "localhost",
    "dbname" => "juanmaaguero-dev",
    "dbuser" => "root",
    "dbpass" => "root",
));

/* views */
Config::set("view", array(
    "path" => __DIR__ . "/../../demo/view/",
    "masterview" => array(
        "frontend" => "frontend",
        "backend" => "backend",
    )
));
Config::set("messages", array(
    "hello" => "hi",
));
?>

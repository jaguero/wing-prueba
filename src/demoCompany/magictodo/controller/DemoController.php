<?php

namespace demoCompany\magictodo\controller;

use demoCompany\magictodo\service\HelloService;
use flowcode\demo\domain\Demo;
use flowcode\demo\service\Container;
use flowcode\demo\service\DemoService;
use flowcode\wing\mvc\controller\BaseController;
use flowcode\wing\mvc\http\HttpRequest;
use flowcode\wing\mvc\view\View;


/**
 * Description of HomeController
 *
 * @author juanma
 */
class DemoController extends BaseController {

    private $helloService;

    public function __construct() {
        $this->setIsSecure(FALSE);
    }

    public function message(HttpRequest $httpRequest) {
        $helloSrv = new HelloService();
        $viewData["message"] = $helloSrv->getHelloMessage();
        return new View($viewData, "backend/demo/message");
    }

    public function index(HttpRequest $httpRequest) {
        $demoSrv = new DemoService();
        $viewData["demos"] = $demoSrv->findAll();
        return new View($viewData, "frontend/demo/list");
    }

    public function create(HttpRequest $httpRequest) {
        $viewData["data"] = "";
        return View::getControllerView($this, "demo/view/demo/create", $viewData);
    }

    public function save(HttpRequest $httpRequest) {
        $demoSrv = new DemoService();
        $demo = new Demo();
        $demo->setName($httpRequest->getParameter("name"));
        $demoSrv->save($demo);
        $this->redirect("/demo");
    }

    public function getHelloService() {
        if (is_null($this->helloService)) {
            $container = new Container();
            $this->helloService = $container->lookup("HelloService");
        }
        return $this->helloService;
    }

    public function setHelloService(HelloService $helloService) {
        $this->helloService = $helloService;
    }

}

?>

<?php

namespace flowcode\demo\service;

/**
 * Description of Container
 *
 * @author Juan Manuel Agüero <jaguero@flowcode.com.ar>
 */
class Container {

    private $content;

    function __construct() {
        $this->content = array(
            "HelloService" => "flowcode\\demo\\service\\HelloService",
        );
    }

    public function lookup($name) {
        if (isset($this->content[$name])) {
            $class = $this->content[$name];
            $instance = new $class();
            return $instance;
        } else {
            return NULL;
        }
    }

}

?>

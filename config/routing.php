<?php

use flowcode\enlace\config\EnlaceRouter;


/* routes */
EnlaceRouter::set("homepage", array("permalink" => "home"));
EnlaceRouter::set("demo", array("controller" => "Demo"));
EnlaceRouter::set("admin", array("controller" => "AdminHome"));
?>

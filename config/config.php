<?php

use flowcode\enlace\config\EnlaceConfig;


/* data base */
EnlaceConfig::set("database", array(
    "driver" => "mysql",
    "mapping" => "orm-mapping.xml",
    "dbserver" => "localhost",
    "dbname" => "juanmaaguero-dev",
    "dbuser" => "root",
    "dbpass" => "root",
));

/* views */
EnlaceConfig::set("view", array(
    "path" => __DIR__ . "/../src/demoCompany/magictodo/view",
    "layout" => array(
        "frontend" => "frontend",
        "backend" => "backend",
    )
));
EnlaceConfig::set("messages", array(
    "hello" => "hi",
));
?>
